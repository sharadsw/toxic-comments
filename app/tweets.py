import tweepy
import os

"""
with open("auth", 'r') as f:
    keys = f.read().split("\n")
    CONSUMER_KEY = keys[0]
    CONSUMER_SECRET = keys[1]
    ACCESS_KEY = keys[2]
    ACCESS_SECRET = keys[3]
"""

CONSUMER_KEY = os.environ.get('CONSUMER_KEY')
CONSUMER_SECRET = os.environ.get('CONSUMER_SECRET')
ACCESS_KEY = os.environ.get('ACCESS_KEY')
ACCESS_SECRET = os.environ.get('ACCESS_SECRET')


# Retrieve tweets using the Twitter API wrapper 'tweepy'
def get_tweets(user, num=25):
    auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
    auth.set_access_token(ACCESS_KEY, ACCESS_SECRET)

    api = tweepy.API(auth)

    tweets = api.user_timeline(screen_name=user, count=num)
    tweets_list = []

    # CSV format for tweets
    tweets_csv = [tweet.text for tweet in tweets]

    # Append those tweets above into a list
    for t in tweets_csv:
        tweets_list.append(t)

    return tweets_list


def main():
    print("User: ")
    user = input()
    print(get_tweets(user))


if __name__ == "__main__":
    main()
