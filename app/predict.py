import sys
import numpy as np
import pickle
import re

from keras.preprocessing.sequence import pad_sequences
from keras.models import load_model

from capsule import Capsule, squash


with open("model/word2idx.pickle", 'rb') as f:
    word2idx = pickle.load(f)

SENTENCE_LENGTH = 100
UNKNOWN_TOKEN = word2idx['UNK']
CLASSES = ["toxic", "severe_toxic", "obscene", "threat", "insult", "identity_hate"]


# Function to preprocess the input text
def clean_text(text):
  # Remove HTML tags
  text = re.sub(r'<.*?>', '', text)
  
  # Remove the characters [\], ['] and ["]
  text = re.sub(r"\\", "", text)    
  text = re.sub(r"\'", "", text)    
  text = re.sub(r"\"", "", text) 
  
  # Convert text to lowercase
  text = text.strip().lower()
  
  # Replace punctuation characters with spaces
  filters='!"\'#$%&()*+,-./:;<=>?@[\\]^_`{|}~\t\n'
  translate_dict = dict((c, " ") for c in filters)
  translate_map = str.maketrans(translate_dict)
  text = text.translate(translate_map)
  
  return text.split()


# Create a word2idx mapping
def make_input(inp):
    final_inp = []
    for i in range(0, len(inp)):
        tk_inp = clean_text(inp[i])
        final_inp.append([word2idx.get(word, UNKNOWN_TOKEN) for word in tk_inp])

    final_inp = pad_sequences(final_inp, maxlen=SENTENCE_LENGTH)

    return final_inp


# Turn the probabilistic output into binary values
def binarize_output(preds):
    probs = np.array(preds)
    labels = (probs > 0.5).astype(np.int)

    return labels


def load():
    print("Loading model")
    print("="*75)
    model = load_model("model/ConvCaps.hdf5", custom_objects={'Capsule': Capsule})
    print("Model loaded into {}".format(model))

    return model


# Predict on the given input
def predictions(inp, model):
    proc_inp = make_input(inp)
    print("Predicting")
    preds = model.predict(proc_inp, verbose=1)

    print(CLASSES)
    del proc_inp, inp

    return binarize_output(preds)


def main():
    inputs = sys.argv[1:]
    inp = [" ".join(inputs)]

    print(predictions(inp))
    

if __name__ == "__main__":
    main()
