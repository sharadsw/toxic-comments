import tweets
import predict

import os
import numpy as np
import tensorflow as tf

from flask import Flask, render_template, request, flash, redirect, url_for
app = Flask(__name__)
app.secret_key = os.environ.get('SECRET_KEY')

g = tf.Graph()
with g.as_default():
    model = predict.load()

# Function to route the index page
@app.route("/", methods = ["GET", "POST"])
def index():
    return render_template("index.html")


# Function to route the analysis page
@app.route("/analysis", methods = ["GET", "POST"])
def analysis():
    if request.method == "POST":
        url = request.form.get('twitter_url')
        num = request.form.get('num_tweets')

        print("=" * 20)
        print(url)
        print(num)
        print("Values obtained")
        print("=" * 20)

        if not "twitter.com" in url:
        	flash("Invalid URL")
        	return redirect(url_for('index'))

        # Obtain Twitter handle from the link given
        url = url.replace("/", "")
        url = url.replace("https:", "")
        url = url.replace("www.", "")
        user = url.replace("twitter.com", "")
        print(user)

		# Get tweets from user
        tw = tweets.get_tweets(user, num)
        print(tw)

        if len(tw) == 0:
        	flash("This Twitter user does not have any tweets")
        	return redirect(url_for('index'))

        # Make predictions
        with g.as_default():
            preds = predict.predictions(tw, model)
            print(preds)

        vals = preds.sum(axis = 0)
        print("Totals: {}".format(vals))

        bkdown = breakdown(preds)
        print("Breakdown matrix: {}".format(bkdown))

        # Get a list of toxic tweets
        tweet_list = []
        for idx in bkdown:
            tweet_list.append(tw[idx])
        print(tweet_list)

        percent = 100*len(tweet_list)/int(len(tw))

        return render_template("analysis.html", vals=vals, tweet_list=tweet_list, percent=int(percent), total=len(tw))

    return render_template("analysis.html")


# Create a mapping of the classes
def breakdown(labels):
    row_values = []

    for i in range(0, 6):
        row_locator = labels[: , i]==1
        for x in range(0, len(labels)):
            if row_locator[x]:
                row_values.append(x)

    # Row_values isn't returned directly so as to avoid duplicate values
    # It was decided at this point in time to not display a detailed tweet breakdown
    # But only those tweets that are deemed toxic under any class
    return list(dict.fromkeys(row_values))